<?php

namespace models;

use BeReborn\Base\Authorize;
use BeReborn\Database\ActiveRecord;
use BeReborn\Exception\AuthException;
use BeReborn\Exception\ComponentException;
use BeReborn\Http\AuthIdentity;
use Exception;

/**
 * Class User
 * @package models
 */
class User extends ActiveRecord implements AuthIdentity
{

	public static $primary = 'id';

	/**
	 * @return string
	 */
	public static function tableName()
	{
		return '{{%user}}';
	}

	/**
	 * @return array
	 * @throws ComponentException
	 * @throws AuthException
	 */
	public function getIdentity(): array
	{
		$auth = \BeReborn::$app->getAuth();

		return $auth->getCurrentOnlineUser();
	}

	/**
	 * @return bool
	 */
	public function checkIdentity(): bool
	{
		return true;
	}


	/**
	 * @return mixed|void
	 * @throws ComponentException
	 * @throws Exception
	 */
	public function createIdentity()
	{
		$auth = \BeReborn::$app->getAuth();

		return $auth->create(get_unique_id('user'));
	}

}
