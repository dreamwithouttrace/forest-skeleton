<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 2019-03-20
 * Time: 10:09
 */

namespace components;


use BeReborn\Core\JSON;
use BeReborn\Error\ErrorHandler;
use BeReborn\Error\Logger;
use BeReborn\Http\Formatter\IFormatter;

/**
 * Class MyErrorHandler
 * @package components
 */
class MyErrorHandler extends ErrorHandler
{

	/**
	 * @param $message
	 * @param $file
	 * @param $line
	 * @param string $category
	 * @param int $code
	 * @return false|mixed|string|IFormatter
	 * @throws \Exception
	 */
	public function sendError($message, $file, $line, $category = 'app', $code = 500)
	{
		$path = ['file' => $file, 'line' => $line];

		$data = JSON::to(500, $category . ': ' . $message, $path);

		Logger::trance($data, $category);

		return \response()->send($data);
	}

}
