<?php


namespace components;


use BeReborn\Service\Process;
use Exception;
use Swoole\Timer;

/**
 * Class Logfilemonitoring
 * @package components
 */
class Leafleting extends Process
{

	/**
	 * @param \Swoole\Process $process
	 * @return mixed|void
	 * @throws Exception
	 */
	public function onHandler(\Swoole\Process $process)
	{
		$server = \BeReborn::$app->getSocket();
		Timer::tick(1000, function () use ($server) {
			$filePath = realpath(\BeReborn::$app->runtimePath . '/log_file.log');
			clearstatcache();
			if (filesize($filePath) > 1024000) {
				@unlink($filePath);
				$server->rePutFile();
			}
			sleep(1);
		});
	}

}
