<?php

namespace App\Tcp;

use BeReborn\Web\Controller;

/**
 * Class ExampleController
 * @package App\Tcp
 */
class ExampleController extends Controller
{

	public function index()
	{
		return 'hello word!';
	}

}
