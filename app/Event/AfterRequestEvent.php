<?php


namespace App\Event;


use BeReborn\Database\Connection;
use BeReborn\Event\IEvent;
use BeReborn\exception\ComponentException;
use BeReborn\Http\Request;

/**
 * Class AfterRequestEvent
 * @package App\Event
 */
class AfterRequestEvent implements IEvent
{

	public $listen = 'AFTER_REQUEST';


	/**
	 * @param mixed ...$params
	 * @return mixed|void
	 */
	public function handler(...$params)
	{
		// TODO: Implement handler() method.

		return true;
	}

}
