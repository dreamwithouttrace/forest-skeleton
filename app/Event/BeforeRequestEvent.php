<?php


namespace App\Event;

use BeReborn\Event\IEvent;
use BeReborn\Http\Request;

/**
 * Class BeforeRequestEvent
 * @package App\Event
 */
class BeforeRequestEvent implements IEvent
{

	public $listen = 'BEFORE_EVENT';

	/**
	 * @param mixed ...$params
	 * @return mixed|void
	 */
	public function handler(...$params)
	{
		// TODO: Implement handler() method.
	}
}
