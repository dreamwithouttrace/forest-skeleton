<?php


namespace App\Udp;

use BeReborn\Web\Controller;

/**
 * Class ExampleController
 * @package App\Tcp
 */
class ExampleController extends Controller
{

	/**
	 * @return string
	 */
	public function index()
	{
		return 'hello word!';
	}

}
