<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 2019-03-19
 * Time: 17:40
 */

namespace App\Middleware;


use BeReborn\Http\Request;
use BeReborn\Route\MiddlewareHandler;

/**
 * Class CheckAuthMiddleware
 * @package App\Middleware
 */
class CheckAuthMiddleware extends MiddlewareHandler
{

	/**
	 * @param Request $request
	 * @param \Closure $next
	 * @return mixed
	 */
	public function handler(Request $request, \Closure $next)
	{
		return $next($request);
	}

}
