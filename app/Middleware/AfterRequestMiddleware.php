<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 2019-03-20
 * Time: 13:07
 */

namespace App\Middleware;



use BeReborn\Http\Request;
use BeReborn\Route\MiddlewareHandler;

/**
 * Class AfterRequestMiddleware
 * @package App\Middleware
 */
class AfterRequestMiddleware extends MiddlewareHandler
{


	/**
	 * @param Request $params
	 * @param \Closure $next
	 * @return mixed
	 */
	public function handler(Request $params, \Closure $next)
	{
		// TODO: Implement handler() method.

		echo \request()->getDebug() . PHP_EOL;
		return $next($params);
	}

}
