<?php


namespace App\Task;


use BeReborn\Task\Task;
use Swoole\WebSocket\Server;

/**
 * Class BrokenLine
 * @package App\Task
 */
class BrokenLine extends Task
{

	const BROKEN_LINE = 'broken_line';
	const ONLINE = 'online';


	/**
	 * @throws \Exception
	 * 掉线通知
	 */
	public function handler()
	{
	}

}
