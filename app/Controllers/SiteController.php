<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 2019-03-25
 * Time: 18:25
 */

namespace App\Controllers;


use BeReborn\Core\JSON;
use BeReborn\Web\Controller;


/**
 * Class SiteController
 * @package app\controller
 */
class SiteController extends Controller
{

	/**
	 * @return mixed
	 * @throws
	 */
	public function index()
	{
		return JSON::to(0, 'Hello Word');
	}




}
