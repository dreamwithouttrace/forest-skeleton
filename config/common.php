<?php

use BeReborn\Http\Response;
use components\MyErrorHandler;
use BeReborn\Base\Config;
use BeReborn\Cache\File;

defined('CONNECT_HOST') or define('CONNECT_HOST', '127.0.0.1');
defined('CONNECT_USER') or define('CONNECT_USER', 'root');
defined('CONNECT_PASS') or define('CONNECT_PASS', 'root');

$config = [
	['cds' => 'mysql:dbname=drifting_bottle;host=' . CONNECT_HOST, 'username' => CONNECT_USER, 'password' => CONNECT_PASS,],
	['cds' => 'mysql:dbname=drifting_bottle;host=' . CONNECT_HOST, 'username' => CONNECT_USER, 'password' => CONNECT_PASS,],
	['cds' => 'mysql:dbname=drifting_bottle;host=' . CONNECT_HOST, 'username' => CONNECT_USER, 'password' => CONNECT_PASS,],
	['cds' => 'mysql:dbname=drifting_bottle;host=' . CONNECT_HOST, 'username' => CONNECT_USER, 'password' => CONNECT_PASS,],
	['cds' => 'mysql:dbname=drifting_bottle;host=' . CONNECT_HOST, 'username' => CONNECT_USER, 'password' => CONNECT_PASS,],
];

return [
	'config'   => [
		'class'      => Config::class,
		'cache_time' => 60 * 60 * 24,
		'model'      => [
			'path'      => APP_PATH . '/models',
			'namespace' => 'models\\'
		],
	],
	'error'    => [
		'class' => MyErrorHandler::class
	],
	'redis'    => [
		'class'     => 'BeReborn\Cache\Redis',
		'host'      => '127.0.0.1',
		'port'      => '6379',
		'prefix'    => 'redis:prefix:',
		'auth'      => 'xl.2005113426',
		'databases' => '0',
	],
//	'memcache' => [
//		'class'   => 'BeReborn\Cache\Memcached',
//		'host'    => '127.0.0.1',
//		'port'    => 11211,
//		'timeout' => 60
//	],
	'db'       => [
		'class'       => 'BeReborn\Database\Connection',
		'id'          => 'db',
		'cds'         => 'mysql:dbname=dbname;host=' . CONNECT_HOST,
		'username'    => CONNECT_USER,
		'password'    => CONNECT_PASS,
		'tablePrefix' => '',

		'maxNumber' => 100,
		'slaveConfig' => [
			'cds' => 'mysql:dbname=dbname;host=' . CONNECT_HOST,
			'username' => CONNECT_USER,
			'password' => CONNECT_PASS
		],
	],
	'qn'       => [
		'class'      => 'BeReborn\Core\Qn',
		'access_key' => '',
		'secret_key' => '',
		'bucket'     => '',
	],
	'response' => [
		'class'  => Response::class,
		'format' => Response::JSON
	]
];
