<?php
/**
 * Created by PhpStorm.
 * User: whwyy
 * Date: 2018/4/26 0026
 * Time: 12:05
 */

use components\Leafleting;
use BeReborn\Service\Http\Server;

$components = array_merge(require_once __DIR__ . '/common.php', [
	'socket' => [
		'class'           => Server::class,
		'host'            => '0.0.0.0',
		'port'            => 6602,
		//		'enableWebSocket' => false,
		'websocket'       => ['0.0.0.0', 6601],
		'socketNamespace' => 'App\\Sockets',
		'socketPath'      => APP_PATH . '/app/Sockets',
		'listens'         => [
			['0.0.0.0', 12001, SWOOLE_UDP],
			['0.0.0.0', 12002, SWOOLE_SOCK_TCP]
		],
		'settings'        => [
			'dispatch_mode'    => 4,
			'reactor_num'      => 8,
			'worker_num'       => 32,
			'backlog'          => 500000,
			//			'hook_flags'       => SWOOLE_HOOK_ALL,
			'max_connection'   => 260000,
			'log_file'         => __DIR__ . '/../runtime/log_file.log',
			'max_request'      => 2000000,
			'task_max_request' => 400000,
			'task_worker_num'  => 50
		],
		'process'         => [
			'leafleting' => Leafleting::class,
		]
	]
]);

return [
	'id'          => 'restful',
	'runtimePath' => __DIR__ . '/../runtime',
	'components'  => $components,

	'aliases' => [

	]
];
