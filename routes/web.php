<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 2019-03-20
 * Time: 02:20
 */

use BeReborn\Route\Router;
use BeReborn\Route\Handler;

/** @var Router $router */
$router->get('/', function () {
	return 'Hello Word';
});

$router->get('/tcp', function () {
	$client = \BeReborn\Http\Client::NewRequest();
	$client->setHost('47.92.194.207');
	$client->setPort('35551');

	$param['id'] = 1;

	$response = $client->sendTo('Example', $param, SWOOLE_TCP);
	if (!$response->isResultsOK()) {
		return $response->getMessage();
	}

	return 'Hello Word';
});
$router->post('/', 'SiteController@index');
$router->listen(35550, function (Handler $router) {
	$router->handler('example', 'ExampleController@index');
	$router->handler('example', 'ExampleController@index');
});
$router->listen(35551, function (Handler $router) {
	$router->handler('example', 'ExampleController@index');
});
